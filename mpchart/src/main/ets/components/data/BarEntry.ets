/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ImagePaint } from './Paint';
import Range from '../highlight/Range';
import EntryOhos from './EntryOhos';


/**
 * Entry class for the BarChart. (especially stacked bars)
 *
 */
export default class BarEntry extends EntryOhos {

  /**
   * the values the stacked barchart holds
   */
  private mYVals: number[];

  /**
   * the ranges for the individual stack values - automatically calculated
   */
  private mRanges: Range[];

  /**
   * the sum of all negative values this entry (if stacked) contains
   */
  private mNegativeSum: number;

  /**
   * the sum of all positive values this entry (if stacked) contains
   */
  private mPositiveSum: number;

  /*constructor(x:number, y:number,  icon:Object*//*Drawable*//*, data:Object) {
    super(y, icon, data);
    this.x = x;
  }*/
  constructor(x?: number, y?: number[]|number,icon?:ImagePaint,  data?:Object) {
    super(x, Array.isArray(y)?BarEntry.calcSum(y):y, icon, data);
    if(Array.isArray(y)){
      this.mYVals = y;
      this.calcPosNegSum();
      this.calcRanges();
    }
  }
  /**
   * Returns an exact copy of the BarEntry.
   */
  public copy(): BarEntry {
    var copied: BarEntry = new BarEntry(this.getX(), this.getY(),null, this.getData());
    copied.setVals(this.mYVals);
    return copied;
  }

  /*
   * Returns the stacked values this BarEntry represents, or null, if only a single value is represented (then, use
   * getY()).
   *
   * @return
   */
  public getYVals(): number[] {
    return this.mYVals;
  }

  /**
   * Set the array of values this BarEntry should represent.
   *
   * @param vals
   */
  public setVals(vals: number[]): void {
    this.setY(BarEntry.calcSum(vals));
    this.mYVals = vals;
    this.calcPosNegSum();
    this.calcRanges();
  }

  /**
   * Returns the value of this BarEntry. If the entry is stacked, it returns the positive sum of all values.
   *
   * @return
   */
  public getY(): number {
    return super.getY();
  }

  /**
   * Returns the ranges of the individual stack-entries. Will return null if this entry is not stacked.
   *
   * @return
   */
  public getRanges(): Range[] {
    return this.mRanges;
  }

  /**
   * Returns true if this BarEntry is stacked (has a values array), false if not.
   *
   * @return
   */
  public isStacked(): boolean{
    return this.mYVals != null;
  }

  /**
   * Use `getSumBelow(stackIndex)` instead.
   */
  public getBelowSum(stackIndex: number): number {
    return this.getSumBelow(stackIndex);
  }

  public getSumBelow(stackIndex: number): number {
    if (this.mYVals == null)
    return 0;
    var remainder: number = 0;

    for(let i = 0;i < stackIndex;i++){
      remainder += this.mYVals[i]
    }
    return remainder;
  }

  /**
   * Reuturns the sum of all positive values this entry (if stacked) contains.
   *
   * @return
   */
  public getPositiveSum(): number {
    return this.mPositiveSum;
  }

  /**
   * Returns the sum of all negative values this entry (if stacked) contains. (this is a positive number)
   *
   * @return
   */
  public getNegativeSum(): number {
    return this.mNegativeSum;
  }

  private calcPosNegSum(): void {

    if (this.mYVals == null) {
      this.mNegativeSum = 0;
      this.mPositiveSum = 0;
      return;
    }

    var sumNeg: number = 0;
    var sumPos: number = 0;

    for (let f of this.mYVals) {
      if (f <= 0)
      sumNeg += Math.abs(f);
      else
      sumPos += f;
    }

    this.mNegativeSum = sumNeg;
    this.mPositiveSum = sumPos;
  }

  /**
   * Calculates the sum across all values of the given stack.
   *
   * @param vals
   * @return
   */
  public static calcSum(vals: number[]): number {

    if (vals == null)
    return 0;
    var sum: number = 0;
    for (let f of vals)
    sum += f;
    return sum;
  }

  protected calcRanges(): void {

    var values: number[] = this.getYVals();

    if (values == null || values.length == 0)
    return;

    this.mRanges = new Array(values.length);

    var negRemain: number = -this.getNegativeSum();
    var posRemain: number = 0;

    for (var i = 0; i < this.mRanges.length; i++) {

      var value: number = values[i];

      if (value < 0) {
        this.mRanges[i] = new Range(negRemain, negRemain - value);
        negRemain -= value;
      } else {
        this.mRanges[i] = new Range(posRemain, posRemain + value);
        posRemain += value;
      }
    }
  }
  public calcYValsMin():number{
    let minNum=0;
    for(let item of this.mYVals){
      minNum=(item>minNum?minNum:item)
    }
    return minNum;
  }
}


