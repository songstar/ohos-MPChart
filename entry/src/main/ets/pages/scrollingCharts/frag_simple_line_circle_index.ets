/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Three from './Three';
import Square from './Square';
import { MyRect } from '@ohos/mpchart';
import { LegendEntry } from '@ohos/mpchart';
import { TextPaint } from '@ohos/mpchart';
import { XAxis,XAxisPosition } from '@ohos/mpchart';
import {YAxis,AxisDependency,YAxisLabelPosition} from '@ohos/mpchart';
import { LineData } from '@ohos/mpchart';
import {LineDataSet,ColorStop,Mode} from '@ohos/mpchart';
import { EntryOhos } from '@ohos/mpchart';
import { JArrayList } from '@ohos/mpchart';
import type { ILineDataSet } from '@ohos/mpchart';
import { YAxisView } from '@ohos/mpchart';
import { PathView,PathViewModel } from '@ohos/mpchart';
import { Utils } from '@ohos/mpchart';
import { MultipleLegend } from '@ohos/mpchart';
import N from './N';
import Nlogn from './Nlogn';
import { LineChartModel } from '@ohos/mpchart';
import { LineChart } from '@ohos/mpchart';

@Entry
@Component
export default struct fragSimpleLineCircleIndex {
  topAxis: XAxis = new XAxis(); //顶部X轴
  bottomAxis: XAxis = new XAxis(); //底部X轴
  mWidth: number = 380; //表的宽度
  mHeight: number = 600; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis = null;
  rightAxis: YAxis = null;
  lineData: LineData = null;
  legendArr:LegendEntry[]=[]
  pathViewModel: PathViewModel = new PathViewModel();
  @State
  lineChartModel: LineChartModel = new LineChartModel();
  build() {
    Column() {
      Stack({ alignContent: Alignment.TopStart }) {
        LineChart({lineChartModel: $lineChartModel})
      }
//      Row() {
//        ForEach(this.legendArr.map((data, index) => {
//          return { index: index, legendItem: data };
//        }), (item) => {
//          Blank().width(4)
//          MultipleLegend({legend:item.legendItem});
//        }, item => item.index)
//      }.margin({left:40})
    }.alignItems(HorizontalAlign.Start)
  }
  aboutToAppear() {
    this.lineData = this.initCurveData(45, 100);

    this.topAxis.setLabelCount(5, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(0);
    this.topAxis.setAxisMaximum(this.lineData.getXMax());
    this.topAxis.setDrawAxisLine(false);
    this.topAxis.setDrawLabels(false);
    this.topAxis.setDrawGridLines(false);

    this.bottomAxis.setLabelCount(5, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(this.lineData.getXMax());
    this.bottomAxis.setDrawAxisLine(false);
    this.bottomAxis.setDrawLabels(false);

    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(7, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setAxisMinimum(this.lineData.getYMin());
    this.leftAxis.setAxisMaximum(this.lineData.getYMax()+10);
    this.leftAxis.setDrawGridLines(true);
    this.leftAxis.setDrawAxisLine(true);
    this.leftAxis.setTextColor(Color.White)
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisLineColor(0x80333333);

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    //this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setAxisMinimum(this.lineData.getYMin()); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(this.lineData.getYMax());
    this.rightAxis.setDrawAxisLine(false);
    this.rightAxis.setDrawLabels(false);

    let textPaint:TextPaint = new TextPaint();
    textPaint.setTextSize(this.leftAxis.getTextSize());

    var leftTextWidth = Utils.calcTextWidth(textPaint,this.getFormattedValue(this.lineData.getYMin()));
    var rightTextWidth = Utils.calcTextWidth(textPaint,this.getFormattedValue(this.lineData.getYMin()));
    let left = this.minOffset + leftTextWidth;
    let top = this.minOffset;
    let right = this.mWidth - this.minOffset - rightTextWidth;
    let bottom = this.mHeight - this.minOffset;
    let myRect:MyRect = new MyRect(left, top, right, bottom);
    this.lineData.mDisplayRect = myRect;

    this.initPathViewModel();

    this.setLegend();

  }

/**
   * 初始化数据
   * @param count  曲线图点的个数
   * @param range  y轴范围
   */
  private initCurveData(count: number, range: number): LineData {

    let valuesN = new JArrayList<EntryOhos>();
    let n:number[]=new N().data;
    for (let i = 0; i < n.length; i++) {
      valuesN.add(new EntryOhos(i, n[i]));
    }

    let valuesNlogn = new JArrayList<EntryOhos>();
    let nlogn:number[]=new Nlogn().data;
    for (let i = 0; i < nlogn.length; i++) {
      valuesNlogn.add(new EntryOhos(i, nlogn[i]));
    }

    let valuesSquare = new JArrayList<EntryOhos>();
    let square:number[]=new Square().data;
    for (let i = 0; i < square.length; i++) {
      valuesSquare.add(new EntryOhos(i, square[i]));
    }

    let valuesThree = new JArrayList<EntryOhos>();
    let three:number[]=new Three().data;
    for (let i = 0; i < three.length; i++) {
      valuesThree.add(new EntryOhos(i, three[i]));
    }

    let dataSet = new JArrayList<ILineDataSet>();

    let set1 = new LineDataSet(valuesN, "O(n)");
    set1.setDrawFilled(false);
    set1.setDrawValues(false);
    set1.setDrawCircles(true);
    set1.setDrawCircleHole(true);
    set1.setCircleColor(0xc0ff8c);
    set1.setCircleRadius(3);
    set1.setCircleHoleRadius(2);
    set1.setMode(Mode.LINEAR);
    set1.setColorByColor(0xc0ff8c);
    set1.setLineWidth(2.5)

    let set2 = new LineDataSet(valuesNlogn, "O(nlogn)");
    set2.setDrawFilled(false);
    set2.setDrawValues(false);
    set2.setDrawCircles(true);
    set2.setDrawCircleHole(true);
    set2.setCircleColor(0xfff78c);
    set2.setCircleRadius(3);
    set2.setCircleHoleRadius(2);
    set2.setMode(Mode.LINEAR);
    set2.setColorByColor(0xfff78c);
    set2.setLineWidth(2.5)

    let set3 = new LineDataSet(valuesSquare, "O(n²)");
    set3.setDrawFilled(false);
    set3.setDrawValues(false);
    set3.setDrawCircles(true);
    set3.setDrawCircleHole(true);
    set3.setCircleColor(0xffd068);
    set3.setCircleRadius(3);
    set3.setCircleHoleRadius(2);
    set3.setMode(Mode.LINEAR);
    set3.setColorByColor(0xffd068);
    set3.setLineWidth(2.5)

    let set4 = new LineDataSet(valuesThree, "O(n³)");
    set4.setDrawFilled(false);
    set4.setDrawValues(false);
    set4.setDrawCircles(true);
    set4.setDrawCircleHole(true);
    set4.setCircleColor(0x8ceaff);
    set4.setCircleRadius(3);
    set4.setCircleHoleRadius(2);
    set4.setMode(Mode.LINEAR);
    set4.setColorByColor(0x8ceaff);
    set4.setLineWidth(2.5)

    dataSet.add(set1);
    dataSet.add(set2);
    dataSet.add(set3);
    dataSet.add(set4);

    return new LineData(dataSet)
  }
  public setLegend(){

    for(let i=0; i<this.lineData.getDataSets().size(); i++) {
      let dataSet = this.lineData.getDataSetByIndex(i)
      let legendLine:LegendEntry =new LegendEntry();
      legendLine.colorHeight=10
      legendLine.colorHeight=10
      legendLine.colorItemSpace=3
      legendLine.colorLabelSpace=4
      legendLine.labelColor=Color.Black
      legendLine.labelTextSize=10
      legendLine.colorArr=dataSet.getColors().dataSouce
      legendLine.label=dataSet.getLabel()
      this.legendArr.push(legendLine)
    }
  }
  private getFormattedValue(value: number): string {
    return value.toFixed(1)
  }

  private initPathViewModel(){
    this.lineChartModel.setTopAxis(this.topAxis);
    this.lineChartModel.setBottomAxis(this.bottomAxis);
    this.lineChartModel.setWidth(this.mWidth);
    this.lineChartModel.setHeight(this.mHeight);
    this.lineChartModel.setMinOffset(this.minOffset);
    this.lineChartModel.setLeftAxis(this.leftAxis);
    this.lineChartModel.setRightAxis(this.rightAxis);
    this.lineChartModel.setLineData(this.lineData);
    this.lineChartModel.init();
  }
}